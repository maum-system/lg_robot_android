package kr.co.july.volley;

import android.content.Context;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class StringRequestJson extends com.android.volley.toolbox.StringRequest {

	Context context;
	String encoding;
	String url;
	boolean noCache = false;
	public StringRequestJson(String url, Map param, Listener<String> listener, ErrorListener errorListener) {
		super(param == null? Method.GET : Method.POST,  url, listener, errorListener);
		encoding = "utf-8";
		this.url = url;
		this.context = VolleyInstance.getContext();
		setRetryPolicy(VolleyInstance.getInstance().getPolicy());
	}
	public StringRequestJson(int method, String url, byte[] body, Listener<String> listener, ErrorListener errorListener) {
		super(method,  url, listener, errorListener);
		encoding = "utf-8";
		this.body = body;
		this.url = url;
		this.context = VolleyInstance.getContext();
		setRetryPolicy(VolleyInstance.getInstance().getPolicy());
	}

	Map<String, String> param = new HashMap<String, String>();

	byte[] body;
	@Override
	public byte[] getBody() throws AuthFailureError {
		if(body != null)
			return body;
		return super.getBody();
	}

	Map<String, String> header = new HashMap<>();
	public Map<String, String> getHeader(){
		return header;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.android.volley.Request#getHeaders()
	 */
	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {

//		header.put("accept", "application/json");
//		header.put("content-type", "application/json");

		return header;
	}

	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response) {
		String parsed;
		try {
			parsed = new String(response.data, encoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			parsed = new String(response.data);
		}
		return Response.success(parsed, null);
	}

}