package kr.co.july.template;

import android.app.Application;

import kr.co.july.devil.core.DevilSdk;
import kr.co.july.template.core.App;
import kr.co.july.volley.VolleyInstance;


/**
 * Created by muyoungko on 2017. 2. 11..
 */

public class MobileApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        VolleyInstance.init(this);

        DevilSdk.init(this);
        DevilSdk.setFirstActivity(FirstActivity.class);
        App.init(this);
    }
}
