package kr.co.july.template.core;

public class Config {
    public static boolean debug = false;
    public static String HOST_API = "https://console-api.deavil.com";
    public static String HOST_WEB = "http://console.deavil.com";
}
