package kr.co.july;

public class Cal {

    public static int getOverCount(int y[], int len, int gap, int cutline){
        if(len < gap)
            return 0;

        int offset = len - gap;
        int r = 0;
        for(int i=offset;i<len;i++)
            if(cutline > y[i])
                r ++;
        return r;
    }

    public static float getAverage(int y[], int len, int gap){
        if(len < gap)
            return 0;

        int offset = len - gap;
        int r = 0;
        for(int i=offset;i<len;i++)
            r += y[i];
        float average = (float)r/gap;
        return average;
    }
    public static double  getRms(int y[], int len, int gap){
        if(len < gap)
            return 0;

        int offset = len - gap;
        double r = 0;
        for(int i=offset;i<len;i++)
            r += y[i] * y[i];
        double average = (float)r/gap;
        average = Math.sqrt(average);
        return average;
    }
    public static float getStandardDistribution(int y[], int len, int gap){
        if(len < gap)
            return 0;

        int offset = len - gap;
        float average = getAverage(y, len, gap);

        float d = 0;
        for(int i=offset;i<len;i++) {
            d += (y[i] - average) * (y[i] - average);
        }
        d = d / gap;
        float sd = (float)Math.sqrt(d);
        return sd;
    }

    public static int getStableAverage(int y[], int len){
        if(len < 10)
            return -1;

        float sd = getStandardDistribution(y, len, 10);
        if(sd < 2.0f){
            return (int)getAverage(y, len, 10);
        } else
            return -1;
    }

    public static float getSlop(int y[], int len, int gap){
        if(len < gap)
            return 0;

        int offset = len - gap;
        float r = (y[len-1] - y[offset]) /3.0f;
        System.out.println("slop = " + r);
        return r;
    }
}
