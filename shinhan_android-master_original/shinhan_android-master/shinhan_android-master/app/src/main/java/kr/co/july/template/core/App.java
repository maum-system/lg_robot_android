package kr.co.july.template.core;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.lge.thirdpartylib.ThirdPartyLib;
import com.lge.thirdpartylib.model.FacilityPoi;

import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import kr.co.july.template.R;
import kr.co.july.volley.StringRequestJson;
import kr.co.july.volley.VolleyInstance;

public class App {
    public static final String DEVIL_PROJECT_ID = "1605234988599";

    private static App instance = null;
    private static Context context = null;
    public static void init(Context context){
        App.context = context;
    }

    public static App getInstance()
    {
        if( instance == null )
            instance = new App();
        return instance;
    }

    String loginToken;
    public App() {
        try {
            initTransMap();
            //pref 가져오기
            SharedPreferences pref = context.getSharedPreferences("pref", context.MODE_PRIVATE);
            //로그인 토큰
            loginToken = pref.getString("TOKEN", null);

            trustAllHosts();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 단말기 고유번호를 반환한다
     * @return 단말기 고유번호
     */
    public String getUdid(){
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public interface HttpCallback{
        void onComplete(boolean success, JSONObject response);
    }

    /**
     * 최초화면으로 가는 챗봇 멘트 호출
     *
     * @param callback
     */
    public void greeting(final HttpCallback callback) {
        chatBotCore("처음으로", callback);
    }

    /**
     * chatBotCore 통신
     * 모든 챗봇 통신(발화, 버튼 클릭 등)은 이 함수를 이용한다
     * @param chat 챗봇 멘트
     * @param callback 응답받을시 호출할 콜백
     */
    public void chatBotCore(final String chat, final HttpCallback callback) {
        try {
            if(chat.equals(""))
                return;
            App.sentIntent = chat;
            Log.i("ChatBotSent", "" + chat);

            String url = "https://serengeti.maum.ai/api.app/app/v2/handle/catalog/instance/lifecycle/executes";
            String body = "lifecycleName=" + URLEncoder.encode("Shinhan_Robot_Chatbot_Lifecycle", "utf-8");
            body += "&target=" + URLEncoder.encode("SoftwareCatalogInstance", "utf-8");
            body += "&async=" + URLEncoder.encode("false", "utf-8");
            body += "&catalogInstanceName=" + URLEncoder.encode("Shinhan_Robot_Chatbot_Catalog", "utf-8");
            JSONObject payload = new JSONObject()
                    .put("host", "1182")
                    .put("session", getUdid())
                    .put("lang", "1")
                    .put("utter", chat);
            body += "&payload=" + URLEncoder.encode(payload.toString(), "utf-8");

            byte postData[] = body.getBytes(StandardCharsets.UTF_8);

            //post
            StringRequestJson request = new StringRequestJson(Request.Method.POST, url, postData, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject res = new JSONObject(response);
                        Log.i("ChatBotSent", "" + res.optString("answer"));
                            callback.onComplete(true, res);
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }
            },
                    new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                Toast.makeText(context, "일시적 네트워크 오류가 발생하였습니다. 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                                    callback.onComplete(false, null);
                            }catch(Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> r = new HashMap<>();
                    r.put("AccessKey", "SerengetiAdministrationAccessKey");
                    r.put("SecretKey", "SerengetiAdministrationSecretKey");
                    r.put("LoginId", "maum-orchestra-com2");
                    r.put("Accept", "application/json");

                    return r;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyInstance.getInstance().getRequestQueue().add(request);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    private Map<String, String> transMap = new HashMap();
    private Map<String, Integer> transIconMap = new HashMap();
    private Map<String, FacilityPoi> transTagMap = new HashMap();

    /**
     * 챗봇의 POI키와 POI의 description에 입력된 태그와의 연결을 정의한다 예)tag1~8
     *
     * transMap
     * 챗봇 키             POI 키
     * ##MOVING_CX존      tag1
     * ...
     *
     * 챗봇의 POI키와 앱에 표출해야할 아이콘간의 키를 정의한다
     * transIconMap
     * 챗봇 키             아이콘 리소스
     * ##MOVING_CX존      R.drawable.ic_puzzle
     *
     */
    public void initTransMap(){
        transMap.put("##MOVING_CX존", "tag1");
        transMap.put("##MOVING_디지로그라운지", "tag2");
        transMap.put("##MOVING_컨시어지데스크", "tag3");
        transMap.put("##MOVING_퀵데스크", "tag4");
        transMap.put("##MOVING_세미나존", "tag5");
        transMap.put("##MOVING_디지털데스크", "tag6");
        transMap.put("##MOVING_밴딩머신", "tag7");
        transMap.put("##MOVING_AI컨시어지", "tag8");
        transMap.put("##MOVING_포토존", "tag9");

        transIconMap.put("##MOVING_CX존", R.drawable.ic_puzzle);
        transIconMap.put("##MOVING_디지로그라운지", R.drawable.ic_rest);
        transIconMap.put("##MOVING_컨시어지데스크", R.drawable.ic_desk);
        transIconMap.put("##MOVING_퀵데스크", R.drawable.ic_thunder);
        transIconMap.put("##MOVING_세미나존", R.drawable.ic_ppt);
        transIconMap.put("##MOVING_디지털데스크", R.drawable.ic_digitaldesk);
        transIconMap.put("##MOVING_밴딩머신", R.drawable.ic_event);
        transIconMap.put("##MOVING_AI컨시어지", R.drawable.ic_concierge);
        transIconMap.put("##MOVING_포토존", R.drawable.ic_photozone);
    }

    /**
     * 챗봇 키를 보고 해당 아이콘을 리소스 번호를 반환한다
     * @param intent 챗봇 키
     * @return
     */
    public Integer getPoiIcon(String intent) {
        return transIconMap.get(intent);
    }

    /**
     * transTagMap 찾기
     * 캣봇 키를 보고 주변 장소를 가져온다
     * 주변 장소가 없을 시 토스트 메세지를 표시한다
     * @param activity
     * @param intent
     * @return transTagMap
     */
    public FacilityPoi getPoiFromMovingIntent(Activity activity, String intent) {
        String tag = transMap.get(intent);
        if(tag == null)
            return null;
        FacilityPoi r = transTagMap.get(tag);
        if(r == null) {
            Toast.makeText(context, "주변 장소를 업데이트 하는 중입니다. 잠시 후 다시 시도해주세요", Toast.LENGTH_LONG).show();
            updatePOI(activity);
        }
        return r;
    }

    /**
     * 주변 장소를 업데이트 하여 받아온후, 각 태그 별로 FacilityPoi 객체를 입력한다
     * @param activity
     */
    public void updatePOI(Activity activity) {
        List<FacilityPoi> poiList = ThirdPartyLib.getPOIList(activity);
        for(int i=0;i<poiList.size();i++) {
            FacilityPoi p = poiList.get(i);
            transTagMap.put(p.getAttributeDesc(), p);
        }
    }

    /**
     * 개발자 화면에서 표시할 챗봇으로부터 받은 문자열
     */
    public static String expectedIntent = "";

    /**
     * 개발자 화면에서 표시할 챗봇에 송신한 문자열
     */
    public static String sentIntent = "";


    /**
     * Trust every server - don't check for any certificate
     */
    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * SSL 사설인증서 오류 회피
     */
    public static class MySSLSocketFactory extends SSLSocketFactory {

        SSLSocketFactory sslSocketFactory;

        public MySSLSocketFactory(SSLSocketFactory sslSocketFactory) {
            super();
            this.sslSocketFactory = sslSocketFactory;
        }

        @Override
        public String[] getDefaultCipherSuites() {
            return sslSocketFactory.getDefaultCipherSuites();
        }

        @Override
        public String[] getSupportedCipherSuites() {
            return sslSocketFactory.getSupportedCipherSuites();
        }

        @Override
        public SSLSocket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
            SSLSocket socket = (SSLSocket) sslSocketFactory.createSocket(s, host, port, autoClose);
            socket.setEnabledProtocols(new String[]{"TLSv1.2"});
            return socket;
        }

        @Override
        public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
            SSLSocket socket = (SSLSocket) sslSocketFactory.createSocket(host, port);
            socket.setEnabledProtocols(new String[]{"TLSv1.2"});
            return socket;
        }

        @Override
        public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException,
                UnknownHostException {
            SSLSocket socket = (SSLSocket) sslSocketFactory.createSocket(host, port, localHost, localPort);
            socket.setEnabledProtocols(new String[]{"TLSv1.2"});
            return socket;
        }

        @Override
        public Socket createSocket(InetAddress host, int port) throws IOException {
            SSLSocket socket = (SSLSocket) sslSocketFactory.createSocket(host, port);
            socket.setEnabledProtocols(new String[]{"TLSv1.2"});
            return socket;
        }

        @Override
        public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort)
                throws IOException {
            SSLSocket socket = (SSLSocket) sslSocketFactory.createSocket(address, port, localAddress, localPort);
            socket.setEnabledProtocols(new String[]{"TLSv1.2"});
            return socket;
        }
    }

}
