package kr.co.july.template;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;

public class ScaleAnimation extends Animation {
    private static final float SPEED = 0.3F;
    private float mStart;
    private float mEnd;
    private View view;



    public ScaleAnimation(View view, float fromX, float toX) {
        this.mStart = fromX;
        this.mEnd = toX;
        this.view = view;
        this.setInterpolator(new DecelerateInterpolator());
        this.setDuration(200L);
    }

    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float offset = (this.mEnd - this.mStart) * interpolatedTime + this.mStart;
        this.view.setScaleX(offset);
        this.view.setScaleY(offset);
    }
}
