package kr.co.july;

import android.app.Activity;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import com.google.api.gax.core.CredentialsProvider;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.rpc.ClientStream;
import com.google.api.gax.rpc.ResponseObserver;
import com.google.api.gax.rpc.StreamController;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.speech.v1.RecognitionAudio;
import com.google.cloud.speech.v1.RecognitionConfig;
import com.google.cloud.speech.v1.RecognizeResponse;
import com.google.cloud.speech.v1.SpeechClient;
import com.google.cloud.speech.v1.SpeechRecognitionAlternative;
import com.google.cloud.speech.v1.SpeechRecognitionResult;
import com.google.cloud.speech.v1.SpeechSettings;
import com.google.cloud.speech.v1.StreamingRecognitionConfig;
import com.google.cloud.speech.v1.StreamingRecognitionResult;
import com.google.cloud.speech.v1.StreamingRecognizeRequest;
import com.google.cloud.speech.v1.StreamingRecognizeResponse;
import com.google.protobuf.ByteString;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//import kr.co.july.devil.core.javascript.JevilInstance;    //cbw 20220419


import kr.co.july.template.BaseActivity;
import kr.co.july.template.FirstActivity;
import kr.co.july.template.R;

/**
 * 발화 녹음을 하고, Text로 변환하기 위한 객체
 *
 * 1. Google Recognizer 제일 빠르다
 * 2. Google Streaming 꽤 빠르다
 * 3. Google STT 제일 느리다
 */
public class DevilRecord {
    private Activity myActivity = null;

    private static final String TAG = "DevilRecord";
    private static DevilRecord instance;


    public static DevilRecord getInstance()
    {
        if(instance == null)
            instance = new DevilRecord();
        return instance;
    }
    public void SetActivity(Activity _myActivity)
    {
        this.myActivity = _myActivity;
    }

    public interface DevilRecordCallback{
        void onComplete(byte[] s);
    }

    private int mAudioSource = MediaRecorder.AudioSource.MIC;
    private int mSampleRate = 16000;
    private int mChannelCount = AudioFormat.CHANNEL_IN_MONO;
    private int mAudioFormat = AudioFormat.ENCODING_PCM_16BIT;
    //private int mBufferSize = AudioTrack.getMinBufferSize(mSampleRate, mChannelCount, mAudioFormat);
    private int mBufferSize = 1024*2;

    public AudioRecord mAudioRecord = null;
    public Thread mRecordThread = null;
    public boolean isRecording = false;
    byte buf[] = new byte[1024*1024*5];
    int buf_index;
    int stream_sent_index;

    int y[] = new int[100000];
    int y_index = 0;
    boolean speak;
    long lastSilent = -1;
    long recordStartTime = -1;
    DevilRecordCallback callback = null;

    /**
     * 녹음을 시작한다
     * 녹음은 계속되며, 구글 STT의 isFinal이벤트에의해 stopRecord가 호출되어 중단된다
     * @param callback 녹음이 완료되었을 때 녹음한 바이트와 함께 콜백함수를 호출한다
     */
    public void startRecord(final DevilRecordCallback callback){

        if(isRecording)
            return;

        Log.d(TAG, "startRecord");

        this.callback = callback;

        isRecording = true;
        buf_index = 0;
        stream_sent_index = 0;
        y_index = 0;
        if(mAudioRecord == null)
            mAudioRecord = new AudioRecord(mAudioSource, mSampleRate, mChannelCount, mAudioFormat, mBufferSize);
        mAudioRecord.startRecording();
        speak = false;
        lastSilent = -1;
        if(mRecordThread != null){
            mRecordThread.interrupt();
        }

        recordStartTime = System.currentTimeMillis();
        mRecordThread = new Thread(new Runnable() {
            @Override
            public void run() {

                while(isRecording) {
                    int ret = mAudioRecord.read(buf, buf_index, mBufferSize);
                    long total = 0;

                    int avr_index = 0;
                    for(int i=0;i<ret-1;i+=100){
                        int s = Math.abs(buf[buf_index+i+1]);
                        s <<= 8;
                        total += Math.abs(s);
                        avr_index ++;
                    }

                    final short sss[] = new short[avr_index];
                    int sss_index = 0;
                    for(int i=0;i<ret-1;i+=100){
                        int s = Math.abs(buf[buf_index+i+1]);
                        s <<= 8;
                        s = Math.abs(s);
                        sss[sss_index++] = (short)s;
                    }

                    if(sss.length == 0) {
                        try{
                            Thread.sleep(1000);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        continue;
                    }
                    //waveformView.setSamples(sss);

                    final int avr = (int)(total/avr_index);
                    y[y_index++] = avr;

                    //double Db = Math.log10(((avr * 10.0) - 327675.0) / 327675.0);
                    double Db = Math.log10(avr);

//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            TextView tv = findViewById(R.id.vol);
//                            tv.setText(avr + "");
//                        }
//                    });

                    buf_index += ret;
                    int gap = 6;
                    if(y_index > gap){
                        double sd = Cal.getStandardDistribution(y, y_index, gap);
                        double rms = Cal.getRms(y, y_index, gap);
//                        Log.d(TAG, buf_index + " read bytes is " + ret
//                                + " avr = " + avr + " Db = " + Db + " sd = " + sd
//                                + " rms = " + rms
//                        );

                        if(avr > 600) {
                            speak = true;
                            lastSilent = -1;
                        }

                        //조용해지면 끝낸다.
                        if(avr < 150 && sd < 100 && (speak == true || System.currentTimeMillis() - recordStartTime > 2000) ){
                            if(lastSilent == -1) {
                                lastSilent = System.currentTimeMillis();
                            }

                            if(System.currentTimeMillis() - lastSilent > 3000) {



/*
//cbw
                                JevilInstance.getCurrentInstance().getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        stopRecord();
                                    }
                                });
*/


                                break;
                            }
                        }
                    }


                    //버퍼사이즈가 초과되며 끝낸다.
                    if(buf_index + mBufferSize > buf.length) {

                        myActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    stopRecord();
                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                        /*
                        //cbw
                        JevilInstance.getCurrentInstance().getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    stopRecord();
                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                         */


                        break;
                    }


                }
            }
        });
        mRecordThread.start();

    }

    /**
     * 안드로이드 녹음 장치를 모두 중단하고 자원을 릴리즈 한다
     */
    public void stopRecord(){
        try{
            Log.d(TAG, "stopRecord");
            isRecording = false;

            if(mRecordThread != null){
                mRecordThread.interrupt();
            }
            mRecordThread = null;
            if(mAudioRecord != null){
                mAudioRecord.stop();
                //mAudioRecord.release();
                //mAudioRecord = null;
            }

            if(buf_index >0 ){
                byte[] aa = new byte[buf_index];
                for(int i=0;i<aa.length;i++){
                    aa[i] = buf[i];
                }
                if(callback != null) {
                    callback.onComplete(buf);
                }
//                byte[] b = SoundUtil.rawToWave(aa, mSampleRate);
            }
            myActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        ((FirstActivity) BaseActivity.instance).init_user_say();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });
            /*
            //cbw
            JevilInstance.getCurrentInstance().getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        ((FirstActivity) BaseActivity.instance).init_user_say();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });
             */
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public interface DevilStringRecognizeCallback{
        void onProgress(String text);
        void onComplete(String text);
    }

    ClientStream<StreamingRecognizeRequest> mClientStream;

    public void sendStreamingIfSttLive(byte[] buf, int offset, int len) {
        if(mClientStream != null) {
            byte[] data = new byte[len];
            System.arraycopy(buf, offset, data, 0, len);
            StreamingRecognizeRequest request = StreamingRecognizeRequest.newBuilder()
                    .setAudioContent(ByteString.copyFrom(data))
                    .build();


            if(mClientStream.isSendReady()) {
                mClientStream.send(request);
//                Log.i(TAG, "sent request " + len  + " bytes");
            }
        }
    }

    /**
     * 음성 녹음을 시작하고, 실시간으로 레코딩을 한다
     * 그리고 Google Cloud STT 실시간 스트리밍 TCP를 연결한다
     *
     * @param context
     * @param callback 발화가 끝났을 때 불려지는 콜백
     * @throws Exception
     */
    public void recordToText(final Context context, final DevilStringRecognizeCallback callback ) throws Exception{
        createSttStreaming(context, callback);
        startRecord(new DevilRecordCallback(){
            @Override
            public void onComplete(byte[] s) {
            }
        });
    }

    /**
     * Google Cloud STT 실시간 스트리밍 TCP연결을 위한 백그라운드 WRAPPER
     * @param context
     * @param callback 발화가 끝났을 때 불려지는 콜백
     * @throws Exception
     */
    public void createSttStreaming(final Context context, DevilStringRecognizeCallback callback) throws Exception{

        new Thread(new Runnable() {
            @Override
            public void run() {
                backgroundStreamingClient(context, callback);
            }
        }).start();
    }

    /**
     * Google Cloud STT 실시간 스트리밍 TCP연결을 한다
     * 구글 stt 라이브러리는 백그라운드 스레드에서 동작해야 한다
     * 구글 Stt 라이브러리를 시작한 스레드는 구글 stt 연결이 알아서 끊기기 전까지는 스레드를 종료해선 안된다
     * 그래서 while(true)로 대기하다, 모든 stt event final(텍스트 변환 완료)가 뜨고 나서 thread를 종료해야한다
     * 그 전에 종료하면 Exception나서 앱이 죽는다
     *
     * @param context
     * @param callback
     */
    public void backgroundStreamingClient(Context context, final DevilStringRecognizeCallback callback) {
        try {
            ResponseObserver<StreamingRecognizeResponse> responseObserver = null;


            CredentialsProvider credentialsProvider = FixedCredentialsProvider.create(
                    ServiceAccountCredentials.fromStream(context.getResources().openRawResource(R.raw.credential))
            );
            SpeechSettings settings = SpeechSettings.newBuilder().setCredentialsProvider(credentialsProvider).build();


            try (SpeechClient client = SpeechClient.create(settings)) {

                responseObserver =
                        new ResponseObserver<StreamingRecognizeResponse>() {
                            ArrayList<StreamingRecognizeResponse> responses = new ArrayList<>();

                            public void onStart(StreamController controller) {}

                            public void onResponse(StreamingRecognizeResponse response) {
                                try {
                                    Log.i(TAG, "response.getSpeechEventType() = " + response.getSpeechEventType());
                                    if(response.getSpeechEventType() == StreamingRecognizeResponse.SpeechEventType.END_OF_SINGLE_UTTERANCE
                                    ) {
                                        stopRecord();
                                    } else if(response.getResultsList().size() > 0) {
                                        StreamingRecognitionResult result = response.getResultsList().get(0);
                                        SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
                                        String recording_text = alternative.getTranscript();
                                        if(response.getResultsList().get(0).getIsFinal()) {
                                            stopRecord();
                                            Log.i(TAG, "onResponse final " + recording_text);
                                            callback.onComplete(recording_text);
                                        } else {
                                            Log.i(TAG, "onResponse progress " + recording_text);
                                            callback.onProgress(recording_text);
                                        }
                                    }
                                } catch(Exception e){
                                    e.printStackTrace();
                                }
                                responses.add(response);
                            }

                            public void onComplete() {
                                for (StreamingRecognizeResponse response : responses) {
                                    if(response.getResultsList().size() > 0) {
                                        StreamingRecognitionResult result = response.getResultsList().get(0);
                                        SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
                                        String recording_text = alternative.getTranscript();

//                                        Log.i(TAG, "recording_text = " + recording_text);

                                    }
                                }
                            }

                            public void onError(Throwable t) {
                                Log.e(TAG, t.toString());
                            }
                        };

                ClientStream<StreamingRecognizeRequest> clientStream =
                        client.streamingRecognizeCallable().splitCall(responseObserver);

                RecognitionConfig recognitionConfig =
                        RecognitionConfig.newBuilder()
                                .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                                .setLanguageCode("ko-KR")
                                .setSampleRateHertz(16000)
                                .build();
                StreamingRecognitionConfig streamingRecognitionConfig =
                        StreamingRecognitionConfig.newBuilder().setConfig(recognitionConfig)
                                .setSingleUtterance(true)
                                .setInterimResults(true)
                                .build();

                StreamingRecognizeRequest request =
                        StreamingRecognizeRequest.newBuilder()
                                .setStreamingConfig(streamingRecognitionConfig)
                                .build(); // The first request in a streaming call has to be a config

                clientStream.send(request);

                mClientStream = clientStream;

                int false_count = 0;
                while(isRecording) {
                    Thread.sleep(300);
                    Log.i(TAG, "mClientStream.isSendReady() = " + mClientStream.isSendReady());
                    if(mClientStream.isSendReady())
                        false_count = 0;
                    else
                        false_count++;
                    if(false_count >= 5)
                        stopRecord();
                    if(stream_sent_index < buf_index) {
                        sendStreamingIfSttLive(buf, stream_sent_index, buf_index - stream_sent_index);
                        stream_sent_index = buf_index;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            responseObserver.onComplete();

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Google Cloud SST인데 스트리밍 방식이 아닌, 완료된 사운드 바이트를 가지고 Text를 변환한다
     * WAV, MP3 등도 가능
     *
     * @param context Context
     * @param s 레코딩 완료된 사운드 바이트
     * @param callback 콜백
     * @throws Exception
     */
    public void byteToText(final Context context, final byte[] s, final DevilStringRecognizeCallback callback ) throws Exception{
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    CredentialsProvider credentialsProvider = FixedCredentialsProvider.create(
                            ServiceAccountCredentials.fromStream(context.getResources().openRawResource(R.raw.credential))
                    );
                    SpeechSettings settings = SpeechSettings.newBuilder().setCredentialsProvider(credentialsProvider).build();
                    try (SpeechClient speechClient = SpeechClient.create(settings)) {

                        // Builds the sync recognize request
                        RecognitionConfig config = RecognitionConfig.newBuilder()
                                .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                                .setSampleRateHertz(16000)
                                .setLanguageCode("ko-KR")
                                .build();
                        ByteString audioBytes = ByteString.copyFrom(s);
                        RecognitionAudio audio = RecognitionAudio.newBuilder()
                                .setContent(audioBytes)
                                .build();


                        //StreamingRecognitionConfig
                        // Performs speech recognition on the audio file
                        RecognizeResponse response = speechClient.recognize(config, audio);
                        List<SpeechRecognitionResult> results = response.getResultsList();

                        for (SpeechRecognitionResult result : results) {
                            // There can be several alternative transcripts for a given chunk of speech. Just use the
                            // first (most likely) one here.
                            SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
                            Log.i(TAG, "Transcription: " + alternative.getTranscript());
                            callback.onComplete(alternative.getTranscript());
                            break;
                        }
                    }
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
