package kr.co.july.template;

import android.app.Application;

//import kr.co.july.devil.core.DevilSdk;    //cbw 20220419
import kr.co.july.template.core.App;
import kr.co.july.volley.VolleyInstance;


/**
 * Created by muyoungko on 2017. 2. 11..
 */

public class MobileApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

/*
        DevilSdk.init(this);
        DevilSdk.setFirstActivity(FirstActivity.class);

 */
        VolleyInstance.init(this);
        App.init(this);

    }
}
