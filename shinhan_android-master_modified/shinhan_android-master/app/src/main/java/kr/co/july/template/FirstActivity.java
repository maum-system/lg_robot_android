package kr.co.july.template;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.lge.thirdpartylib.ThirdPartyLib;
import com.lge.thirdpartylib.model.FacilityPoi;

import org.json.JSONArray;
import org.json.JSONObject;

import kr.co.july.DevilRecord;

//import kr.co.july.devil.core.DevilUtil;
/*
//cbw   20220419
import kr.co.july.devil.core.DevilSdk;
import kr.co.july.devil.core.DevilUtil;
import kr.co.july.devil.core.ani.AlphaAnimation;
import kr.co.july.devil.core.javascript.JevilInstance;
import kr.co.july.devil.core.permission.DevilPermission;
import kr.co.july.devil.extra.FlexScreen;
 */


import kr.co.july.template.core.App;

//public class FirstActivity extends BaseActivity implements App.HttpCallback, View.OnClickListener , FirstActivity.PlayEndCallback {
public class FirstActivity extends BaseActivity implements App.HttpCallback, View.OnClickListener {
    public static final String TAG = "FirstActivity";

    TextView bubbleText;
    View bubbleBg;
    boolean movingStopped = false;
    private WebView mWebView; // 웹뷰 선언
    private WebSettings mWebSettings; //웹뷰세팅

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        play_wait();
    }

    /**
     * 화면,
     * 버튼,
     * 웹뷰,
     * 초기화 및 설정
     */
    public void init() {
        /**
         * 폰용 XML
         * R.layout.activity_first_phone
         * 로봇 및 테블릿용 XML
         * R.layout.activity_first
         */
        setContentView(R.layout.activity_first_phone);
        //setContentView(R.layout.activity_first);
        DevilRecord.getInstance().SetActivity(this);


/*
//cbw
        FlexScreen.init(this);
        DevilSdk.setFirstActivity(FirstActivity.class);
        DevilSdk.setNotificationIconResource(R.mipmap.ic_launcher_round);
*/

        // 웹뷰 설정
        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebSettings = mWebView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setSupportMultipleWindows(false);
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(false);
        mWebSettings.setLoadWithOverviewMode(true);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setBuiltInZoomControls(false);
        mWebSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebSettings.setDomStorageEnabled(true);

        bubbleText = findViewById(R.id.bubbleText);
        bubbleBg = findViewById(R.id.bubbleBg);

        findViewById(R.id.debug).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FirstActivity.this, DebugActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

/*
//cbw
        DevilPermission.getInstance().request(this, new String[]{Manifest.permission.RECORD_AUDIO}, new DevilPermission.PermissionCallback() {
            @Override
            public void onComplete(boolean success) {

            }
        });
*/
        findViewById(R.id.btn1).setOnClickListener(this);
        findViewById(R.id.btn2).setOnClickListener(this);
        findViewById(R.id.btn3).setOnClickListener(this);

        findViewById(R.id.btn1Type2).setOnClickListener(this);
        findViewById(R.id.btn2Type2).setOnClickListener(this);

        findViewById(R.id.btngroup3_1).setOnClickListener(this);
        findViewById(R.id.btngroup3_2).setOnClickListener(this);
        findViewById(R.id.btngroup3_3).setOnClickListener(this);
        findViewById(R.id.btngroup3_4).setOnClickListener(this);
        findViewById(R.id.btngroup3_5).setOnClickListener(this);
        findViewById(R.id.btngroup3_6).setOnClickListener(this);
        findViewById(R.id.btngroup3_7).setOnClickListener(this);
        findViewById(R.id.btngroup3_8).setOnClickListener(this);
        findViewById(R.id.btngroup3_9).setOnClickListener(this);

        findViewById(R.id.btngroup4_1).setOnClickListener(this);
        findViewById(R.id.btngroup4_2).setOnClickListener(this);
        findViewById(R.id.btngroup4_3).setOnClickListener(this);
        findViewById(R.id.btngroup4_4).setOnClickListener(this);
        findViewById(R.id.btngroup4_5).setOnClickListener(this);
        findViewById(R.id.btngroup4_6).setOnClickListener(this);

        findViewById(R.id.btnGType).setOnClickListener(this);

        findViewById(R.id.webview_btn1).setOnClickListener(this);
        findViewById(R.id.webview_btn2).setOnClickListener(this);

        findViewById(R.id.logo).setOnClickListener(this);
        findViewById(R.id.mike).setOnClickListener(this);
        findViewById(R.id.dev).setOnClickListener(this);

        findViewById(R.id.view).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                waitRelease();
                return false;
            }
        });
    }

    /**
     * 홈화면으로 이동하면서,
     * 마이크 상태를 초기화한다
     */
    public void home() {
        App.getInstance().greeting(this);
        init_user_say();
    }

    /**
     * 현재 플레이 중인 인공인간을(R.id.mp4_2)
     * 대기 화면 상태(R.id.mp4)로 만든다
     */
    public void play_wait() {
        try{
            final VideoView vv2 = findViewById(R.id.mp4_2);
            vv2.pause();
            vv2.setVisibility(View.GONE);

            final VideoView vv = findViewById(R.id.mp4);
            vv.setVisibility(View.VISIBLE);
            vv.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.stm_wait));
            vv.animate().alpha(1.0f);
            vv.start();
            vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 현재 mp4_2에서 플레이되고 있는 url이며, 이를 통해 같은 URL이 되풀이되어 플레이되는 것을 방지한다
     */
    String currentPlayUrl = null;

    /*
    @Override
    public void onEnd() {
        Toast.makeText(this, "영상 종료", Toast.LENGTH_SHORT).show();
        //Log.d(TAG,"영상 종료 이벤트 입니다.");
    }
*/
    interface PlayEndCallback {
        void onEnd();
    }

    /**
     * 인공인간 재생(mp4_2)
     *
     * @param url 재생할 url이며, 챗봇이 전달준다. 이 url은 로컬 mp4파일과 매칭되며, 로컬 mp4파일이 없을 떄 온라인으로 재생한다
     * @param playEndCallback 재생이 완료되면 나올 callback
     */
    public void play(String url, PlayEndCallback playEndCallback) {
        try{
            final VideoView vv = findViewById(R.id.mp4_2);

            /**
             * 같은 URL이 현재 플레이 중이면 다시 플레이 하지 않는다
             */
            if(url == null || (url.equals(currentPlayUrl) && vv.isPlaying()))
                return ;

            currentPlayUrl = url;

            if(url == null)
                return ;

            /**
             * mp4_2를 GONE->VISIBLE로 바꿔줘야 재생이 된다
             * GONE상태에서는 재생되지 않는다
             */
            vv.setVisibility(View.VISIBLE);
            vv.setVideoURI(Uri.parse(url));

            /**
             * 스트리밍으로 플레이될 준비가 되었으면, 이 함수가 콜된다
             */
            vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    /**
                     * 대기화면은 일단 looping을 멈춰서 처음부터 플레이되도록 만든다
                     */
                    final VideoView mp4 = findViewById(R.id.mp4);
                    mp.setLooping(false);

                    /**
                     * 대기화면(R.id.mp4)과 인공인간(R.id.mp4_2)사이에 있는 가림막(cutton)이 보이는 상태라면, (즉, 최초 앱 실행)
                     * 대기화면을 position 0으로 옮긴후 대기(pause)하여, 인공인간 재생이 끝나면 바로 대기화면 00:00 부터 재생되도록 준비한다
                     *
                     * 그렇기 않으면 대기화면 프레임이 튄다
                     */
                    if(findViewById(R.id.cutton).getVisibility() == View.VISIBLE) {
                        vv.start();
                        mp4.seekTo(0);
                        mp4.pause();
                    } else {
                        /**
                         * 현재 대기화면의 재생시간을 2초 단위로 구간을 구해,
                         * 남은 구간 후에 인공인간이 맞춰서 재생되도록 한다
                         *
                         * 인공인간이 재생되는 순간 대기화면은 00:00으로 대기시킨다
                         */
                        int mp4_current = mp4.getCurrentPosition();
                        findViewById(R.id.view).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                vv.setVisibility(View.VISIBLE);
                                vv.start();

                                mp4.seekTo(0);
                                mp4.pause();
                            }
                        }, 2000 - (mp4_current % 2000));
                    }
                }
            });

            /**
             * 인공인간 재생이 끝나면, 가림막을 영원히 숨기고(대기화면이 이제 로딩된상태고, 검은 깜짝임이 안보이기 떄문에)
             * 그 끝에 맞춰 다시 대기화면을 재생하여 프레임을 이어지도록 한다
            */
            vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    vv.setVisibility(View.GONE);
                    findViewById(R.id.cutton).setVisibility(View.GONE);
                    final VideoView mp4 = findViewById(R.id.mp4);
                    mp4.resume();
                    mp4.start();
                    if(playEndCallback != null) {
                        playEndCallback.onEnd();
                    }
                }
            });
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    long lastDevClickTime = -1;
    int devClickCount = 0;
    /**
     * 개발자 모드 진입 방법
     * 언어 버튼 5회 연타 시 개발자 모드 진입
     */
    public void devClick() {

        if(System.currentTimeMillis() - lastDevClickTime < 1000) {
            devClickCount++;
            if(devClickCount == 4) {
                Intent intent = new Intent(FirstActivity.this, DebugActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        } else {
            devClickCount = 0;
        }

        lastDevClickTime = System.currentTimeMillis();
    }

    long lastClick = -1;

    /**
     * 클릭이벤트
     * @param v
     */
    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.dev:
                    devClick();
                    return;
            }
            if(System.currentTimeMillis() - lastClick < 1000) {
                return;
            }
            lastClick = System.currentTimeMillis();

            waitRelease();
            switch (v.getId()) {
                /**
                 * 로고 클릭 혹은 웹뷰 닫기시 처음으로 이동
                 */
                case R.id.logo:
                case R.id.webview_btn2:
                    home();
                    break;

                /**
                 * 모든 형태의 챗봇 버튼
                 */
                case R.id.webview_btn1:
                case R.id.btn1:
                case R.id.btn2:
                case R.id.btn3:
                case R.id.btn1Type2:
                case R.id.btn2Type2:
                case R.id.btngroup3_1:
                case R.id.btngroup3_2:
                case R.id.btngroup3_3:
                case R.id.btngroup3_4:
                case R.id.btngroup3_5:
                case R.id.btngroup3_6:
                case R.id.btngroup3_7:
                case R.id.btngroup3_8:
                case R.id.btngroup3_9:
                case R.id.btngroup4_1:
                case R.id.btngroup4_2:
                case R.id.btngroup4_3:
                case R.id.btngroup4_4:
                case R.id.btngroup4_5:
                case R.id.btngroup4_6:
                case R.id.btnGType:
                {
                    if(v.getId() == R.id.btn1)
                        circleClick(R.id.btn1circle1, R.id.btn1circle2);
                    else if(v.getId() == R.id.btn2)
                        circleClick(R.id.btn2circle1, R.id.btn2circle2);
                    else if(v.getId() == R.id.btn3)
                        circleClick(R.id.btn3circle1, R.id.btn3circle2);

                    JSONObject expectedIntent = (JSONObject)v.getTag();
                    String intent = expectedIntent.optString("intent");

                    /**
                     * 같이 사진찍기에 대한 챗봇 키
                     */
                    if(intent.startsWith("##picture")) {
                        ThirdPartyLib.requestPhotoService(this);
                    } else {
                        /**
                         * 길안내 중지에 대한 챗봇키
                         * 인공인간 재생이 끝나면 길안내가 시작되는데
                         * 그전에 movingStopped가 false가 되면 길안내를 하지 않는다
                         */
                        if(intent.startsWith("##길안내중지")) {
                            movingStopped = true;
                        }
                        App.getInstance().chatBotCore(intent, this);
                    }
                    break;
                }
                /**
                 * 발화 시작
                 */
                case R.id.mike: {
                    play_wait();
                    voice();
                    findViewById(R.id.cutton).setVisibility(View.GONE);
                    break;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     * TYPE B에 대한 버튼 에니메이션 처리
     * 알파 에니메이션 0 -> 1 -> 0
     * @param circle1 버튼의 테투리 1
     * @param circle2 버튼의 테투리 2
     */
    public void circleClick(int circle1, int circle2){
        AnimationSet ani = new AnimationSet(true);
/*
//cbw
        ani.addAnimation(new AlphaAnimation(findViewById(circle1), 0, 1));
        ani.addAnimation(new AlphaAnimation(findViewById(circle2), 0, 1));
*/
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                AnimationSet ani2 = new AnimationSet(true);

                /*
                Animation anim = new AlphaAnimation(0.0f,1.0f);
                anim.setDuration(100);
                anim.setStartOffset(20);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);


                View a = (View)findViewById(circle1);
                a.startAnimation(anim);
                View b = (View)findViewById(circle2);
                b.startAnimation(anim);


                //(View)findViewById(circle2).startAnimation(anim);
                */
//cbw
/*
                ani2.addAnimation(new AlphaAnimation(findViewById(circle2), 1, 0));
                ani2.addAnimation(new AlphaAnimation(findViewById(circle1), 1, 0));
*/

                //findViewById(circle1).startAnimation(ani2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        findViewById(circle1).startAnimation(ani);
    }

    /**
     * 마이크 초기화
     */
    public void init_user_say() {
        if(findViewById(R.id.user_say) != null) {
            ((TextView) findViewById(R.id.user_say)).setTypeface(null, Typeface.NORMAL);
            ((TextView) findViewById(R.id.user_say)).setText("마이크 버튼을 선택하시면\n음성대화를 시작할 수 있습니다.");
        }
    }

    /**
     * 보이스 버튼 애니메이션 이벤트
     */
    public void voiceAnimationStart() {
        ((TextView)findViewById(R.id.user_say)).setText("");
        //findViewById(R.id.mic_circle1).startAnimation(new RepeatAnimation());
    }

    /**
     * 보이스 버튼 애니메이션 이벤트
     */
    public void voiceAnimationStop() {
        AnimationSet ani = new AnimationSet(true);
        ani.addAnimation(new ScaleAnimation(findViewById(R.id.mic_circle3), 1.3f, 1));
        findViewById(R.id.mic_circle1).clearAnimation();
    }


    public static void playSound(Context context, int rawresource) throws Exception {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, rawresource);
        mediaPlayer.setVolume(1.0F, 1.0F);
        mediaPlayer.setLooping(false);
        mediaPlayer.start();
    }


    /**
     * 발화 시작
     */
    public void voice() {
        try{
            /**
             * 발화 에니메이션 시작
             */
            voiceAnimationStart();
            /**
             * 발화 효과음
             */
            playSound(this, R.raw.devil_camera_record);

            /**
             * 현재 발화 중이면 Google STT가 끝낼때까지 기다려야한다
             */
            if(DevilRecord.getInstance().isRecording) {
                Toast.makeText(FirstActivity.this, "음성인식 준비 중입니다 잠시만 기다려주세요", Toast.LENGTH_SHORT).show();
            } else {
                DevilRecord.getInstance().recordToText(getApplicationContext(), new DevilRecord.DevilStringRecognizeCallback() {


                    //음성 인식 텍스트 user_say 표시
                    @Override
                    public void onProgress(String text) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    ((TextView) findViewById(R.id.user_say)).setText(text);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                    //받은 음성인식으로 chatBotCore
                    @Override
                    public void onComplete(String text) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    voiceAnimationStop();
                                    //DevilUtil.playSound(FirstActivity.this, kr.co.july.devil.R.raw.devil_camera_record);
                                    playSound(FirstActivity.this, R.raw.devil_camera_record);
                                    ((TextView) findViewById(R.id.user_say)).setText(text);
                                    ((TextView) findViewById(R.id.user_say)).setTypeface(null, Typeface.BOLD);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        App.getInstance().chatBotCore(text, FirstActivity.this);
                    }
                });
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 모든 챗봇 응답은 이 함수로 받는다
     * 챗봇 응답에 따라, 대답 및 버튼 모양, 순서, 웹뷰, 재생할 인공인간 등을 설정한다
     *
     * @param success 네트워크 성공 여부
     * @param response 쳇봇 응답 json
     */
    @Override
    public void onComplete(boolean success, JSONObject response) {
        try {
            waitRelease();

            bubbleBg.setVisibility(View.VISIBLE);
            String answer = response.optJSONObject("answer").optString("answer").replaceAll("<br>","\n").replaceAll("\\\\n","\n");;
            bubbleText.setText(answer);
            String weburl = response.optString("weburl");
            String mediaurl = response.optString("mediaurl");
            JSONArray expectedIntents = response.optJSONArray("expectedIntents");
            String road = expectedIntents.optJSONObject(0).optString("intent");

            String type = "A";
            if(weburl != null && !weburl.equals("None")) { //길찾기 상세
                type = "F";
            } else if(expectedIntents.length() == 3) {  //메인
                type = "B";
            } else if(expectedIntents.length() == 9) { //길찾기 메뉴
                type = "D";
            } else if(expectedIntents.length() == 2) {  //번호표
                type = "C";
            } else if(road.equals("##길안내중지")) { //길안내 중지
                type = "G";
            }else { //그외 상황
                type = "E";
            }

            App.expectedIntent =  expectedIntents.toString() + "\n" +
                    "weburl = " + weburl + "\n" +
                    "mediaurl = " + mediaurl + "\n" +
                    "MemoryIntent = " + response.optJSONObject("setMemory").optJSONObject("intent").optString("intent");

            final boolean movingCommand = response.optJSONObject("setMemory").optJSONObject("intent").optString("intent").startsWith("##MOVING");
            final boolean photoCommand = response.optJSONObject("setMemory").optJSONObject("intent").optString("intent").startsWith("##PIC");

            //타입에 맞는 메인 비디오 교체
            if(mediaurl != null && !mediaurl.equals("")) {
                String fileName = mediaurl.substring(mediaurl.lastIndexOf("/")+1);
                fileName = fileName.substring(0, fileName.indexOf("."));
                int id = this.getResources().getIdentifier("s"+fileName, "raw", this.getPackageName());
                if(id > 0)
                    mediaurl = "android.resource://" + getPackageName() + "/" + id;

                play(mediaurl, new PlayEndCallback() {
                    @Override
                    public void onEnd() {
                        if(movingCommand && !movingStopped) {
                            String movingIntent = response.optJSONObject("setMemory").optJSONObject("intent").optString("intent");
                            FacilityPoi poi = App.getInstance().getPoiFromMovingIntent(FirstActivity.this, movingIntent);
                            if(poi != null) {
                                cancelRelease();
                                ThirdPartyLib.requestEscort(FirstActivity.this, poi.getPoiId());
                            }
                        } else if(photoCommand) {
                            ThirdPartyLib.requestPhotoService(FirstActivity.this);
                        }

                    }
                });
            }

            // 버튼 및 레이아웃 GONE
            findViewById(R.id.btn1circle1).setAlpha(0.0f);
            findViewById(R.id.btn1circle2).setAlpha(0.0f);
            findViewById(R.id.btn2circle1).setAlpha(0.0f);
            findViewById(R.id.btn2circle2).setAlpha(0.0f);
            findViewById(R.id.btn3circle1).setAlpha(0.0f);
            findViewById(R.id.btn3circle2).setAlpha(0.0f);
            findViewById(R.id.btnGroup1).setVisibility(View.GONE);
            findViewById(R.id.btnGroup2).setVisibility(View.GONE);
            findViewById(R.id.btnGroup3).setVisibility(View.GONE);
            findViewById(R.id.btnGroup4).setVisibility(View.GONE);
            findViewById(R.id.btnGroupWebView).setVisibility(View.GONE);
            findViewById(R.id.blank).setVisibility(View.GONE);

            if(movingCommand) {
                movingStopped = false;
            }

            int btn[] = new int[] {};
            int btnText[] = new int[] {};
            int btnText2[] = new int[] {};

            if(type.equals("B")) {  //메인 레이아웃
                btn = new int[] {R.id.btn1, R.id.btn2, R.id.btn3};
                btnText = new int[] {R.id.btn1Text, R.id.btn2Text, R.id.btn3Text};
                findViewById(R.id.btnGroup1).setVisibility(View.VISIBLE);
                for(int i=0;i<expectedIntents.length() && i<btn.length;i++) {
                    JSONObject expectedIntent = expectedIntents.optJSONObject(i);
                    String displayName = expectedIntent.optString("displayName").replaceAll("<br>","\n").replaceAll("\\\\n","\n");
                    ((TextView) findViewById(btnText[i])).setText(displayName.trim());
                    findViewById(btn[i]).setTag(expectedIntent);
                    findViewById(btn[i]).setVisibility(View.VISIBLE);
                }
            } else if(type.equals("C")) {   //번호표 레이아웃
                findViewById(R.id.btnGType).setVisibility(View.GONE);
                btn = new int[] {R.id.btn1Type2, R.id.btn2Type2};
                btnText = new int[] {R.id.btn1TextType2, R.id.btn2TextType2};
                btnText2 = new int[] {R.id.btn1TextType22, R.id.btn2TextType22};
                findViewById(R.id.btnGroup2).setVisibility(View.VISIBLE);

                for(int i=0;i<expectedIntents.length() && i<btn.length;i++) {
                    JSONObject expectedIntent = expectedIntents.optJSONObject(i);
                    String displayName = expectedIntent.optString("displayName");
                    String dd[] = displayName.split(", ");
                    ((TextView) findViewById(btnText[i])).setText(dd[0]);
                    ((TextView) findViewById(btnText2[i])).setText(dd.length>1?dd[1]:"");
                    findViewById(btn[i]).setTag(expectedIntent);
                    findViewById(btn[i]).setVisibility(View.VISIBLE);
                }
            } else if(type.equals("D")) {   //길찾기 메뉴
                findViewById(R.id.btnGroup3).setVisibility(View.VISIBLE);

                btn = new int[] {R.id.btngroup3_1, R.id.btngroup3_2, R.id.btngroup3_3,
                        R.id.btngroup3_4, R.id.btngroup3_5, R.id.btngroup3_6,
                        R.id.btngroup3_7, R.id.btngroup3_8, R.id.btngroup3_9,
                };

                for(int i=0;i<btn.length;i++) {
                    findViewById(btn[i]).setVisibility(View.GONE);
                }

                for(int i=0;i<expectedIntents.length() && i<btn.length;i++) {
                    JSONObject expectedIntent = expectedIntents.optJSONObject(i);
                    String displayName = expectedIntent.optString("displayName").replaceAll("<br>","\n").replaceAll("\\\\n","\n");
                    ViewGroup btnGroup = findViewById(btn[i]);
                    ((TextView) btnGroup.findViewById(R.id.text)).setText(displayName);
                    Integer icon_id = App.getInstance().getPoiIcon(expectedIntent.optString("intent"));
                    if(icon_id != null) {
                        ImageView icon = btnGroup.findViewById(R.id.img);
                        icon.setImageResource(icon_id);
                    }

                    findViewById(btn[i]).setTag(expectedIntent);
                    findViewById(btn[i]).setVisibility(View.VISIBLE);
                }
            } else if(type.equals("E")) {   //궁금증 메뉴
                findViewById(R.id.btnGroup4).setVisibility(View.VISIBLE);
                btn = new int[] {R.id.btngroup4_1, R.id.btngroup4_2, R.id.btngroup4_3,
                        R.id.btngroup4_4, R.id.btngroup4_5, R.id.btngroup4_6
                };

                for(int i=0;i<btn.length;i++) {
                    findViewById(btn[i]).setVisibility(View.GONE);
                }

                for(int i=0;i<expectedIntents.length() && i<btn.length;i++) {
                    JSONObject expectedIntent = expectedIntents.optJSONObject(i);
                    String displayName = expectedIntent.optString("displayName").replaceAll("<br>","\n").replaceAll("\\\\n","\n");
                    ViewGroup btnGroup = findViewById(btn[i]);
                    ((TextView) btnGroup.findViewById(R.id.text)).setText(displayName);
                    Integer icon_id = App.getInstance().getPoiIcon(expectedIntent.optString("intent"));
                    if(icon_id != null) {
                        ImageView icon = btnGroup.findViewById(R.id.img);
                        icon.setImageResource(icon_id);
                    }

                    findViewById(btn[i]).setTag(expectedIntent);
                    findViewById(btn[i]).setVisibility(View.VISIBLE);
                }
            } else if(type.equals("F")) {   //길찾기 상세
                findViewById(R.id.btnGroupWebView).setVisibility(View.VISIBLE);

                if(expectedIntents.length() == 2) {
                    findViewById(R.id.webview_btn1).setVisibility(View.VISIBLE);
                    findViewById(R.id.webview_btn1).setTag(expectedIntents.optJSONObject(0));

                    findViewById(R.id.webview_btn2).setTag(expectedIntents.optJSONObject(1));
                } else {
                    findViewById(R.id.webview_btn1).setVisibility(View.GONE);
                    findViewById(R.id.webview_btn2).setTag(expectedIntents.optJSONObject(0));
                }

                mWebView.loadUrl(weburl); // 웹뷰에 표시할 웹사이트 주소, 웹뷰 시작
            } else if(type.equals("G")) {   //길찾기 종료
                findViewById(R.id.btnGroup2).setVisibility(View.VISIBLE);
                btn = new int[] {R.id.btnGType, R.id.btn2Type2, R.id.btn1Type2};
                for(int i=0;i<btn.length;i++) {
                    findViewById(btn[i]).setVisibility(View.GONE);
                }

                for(int i=0;i<expectedIntents.length() && i<btn.length;i++) {
                    JSONObject expectedIntent = expectedIntents.optJSONObject(i);
                    String displayName = expectedIntent.optString("displayName").replaceAll("<br>","\n").replaceAll("\\\\n","\n");
                    ViewGroup btnGroup = findViewById(btn[i]);
                    ((TextView) btnGroup.findViewById(R.id.btnGTextType)).setText(displayName);

                    findViewById(btn[i]).setTag(expectedIntent);
                    findViewById(btn[i]).setVisibility(View.VISIBLE);
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    long lastOnPauseTime = System.currentTimeMillis();
    @Override
    protected void onResume() {
        super.onResume();
        try {
            //JevilInstance.getCurrentInstance().setActivity(this);
            App.getInstance().updatePOI(this);
            home();
            play_wait();
        }catch(Exception e){

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            lastOnPauseTime = System.currentTimeMillis();
            DevilRecord.getInstance().stopRecord();
            cancelRelease();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    Runnable releaseRunnable = new Runnable() {
        @Override
        public void run() {
            ThirdPartyLib.onUserInteractionTimeout(FirstActivity.this);
        }
    };

    /**
     * 걸어 놓은 LG앱 호출을 취소한다
     */
    public void cancelRelease() {
        findViewById(R.id.view).removeCallbacks(releaseRunnable);
    }

    /**
     * LG앱 호출을 25초 후에 걸어놓는다
     */
    public void waitRelease() {
        findViewById(R.id.view).removeCallbacks(releaseRunnable);
        findViewById(R.id.view).postDelayed(releaseRunnable, 25000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRelease();
    }
}
