package kr.co.july.template;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lge.thirdpartylib.ThirdPartyLib;
import com.lge.thirdpartylib.model.FacilityPoi;

import java.util.List;
//import kr.co.july.devil.core.javascript.JevilInstance;    //cbw 20220419
import kr.co.july.template.core.App;


/**
 * 개발자 화면이며, LG연동 및 주변 POI정도 표출, 해상도 등 직접 연동을 해볼 수 있고,
 * 그 결과를 볼 수 있다
 */
public class DebugActivity extends BaseActivity {
    TextView result;
    View guide;
    LinearLayout guide_list;
    final static String TAG = "DebugActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug);

        result = findViewById(R.id.result);
        guide = findViewById(R.id.guide);
        guide_list = findViewById(R.id.guide_list);

        findViewById(R.id.back).setOnClickListener(click);
        findViewById(R.id.btn1).setOnClickListener(click);
        findViewById(R.id.btn2).setOnClickListener(click);
        findViewById(R.id.btn3).setOnClickListener(click);
        findViewById(R.id.btn4).setOnClickListener(click);
        findViewById(R.id.btn5).setOnClickListener(click);

        result.setText(App.sentIntent + "\n\n" + App.expectedIntent);
        result.setTextIsSelectable(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //JevilInstance.getCurrentInstance().setActivity(this); //cbw
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try{
                switch (v.getId()) {
                    case R.id.back:
                        finish();
                        break;
                    case R.id.btn1:
                        btn1();
                        break;
                    case R.id.btn2:
                        btn2();
                        break;
                    case R.id.btn3:
                        btn3();
                        break;
                    case R.id.btn4:
                        btn4();
                        break;
                    case R.id.btn5:
                        btn5();
                        break;
                }

            }catch(Exception e){
                e.printStackTrace();
                error(e);
            }
        }
    };

    /**
     * LG 기기와 연동하여 주변 POI 목록을 가져온다
     */
    public void btn1() {
        List<FacilityPoi> poiList = ThirdPartyLib.getPOIList(this);
        guide.setVisibility(View.VISIBLE);
        guide_list.removeAllViews();
        for (FacilityPoi poi : poiList) {
            Log.d(TAG, "poi id:" + poi.getPoiId() + ", name:" + poi.getNameKr());
            if(poi.getAttributeDesc() != null && poi.getAttributeDesc().startsWith("tag"))
                addGuideButton(poi.getPoiId(), poi.getNameKr());
        }
    }

    /**
     * 결과로나온 POI 에 대해 가이드버튼 추가 하기
     * @param id 각 POI ID
     * @param name 각 POI 명칭
     */
    public void addGuideButton(String id, String name) {
        Button button = new Button(this);
        button.setText(name);
        button.setTag(id);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                try{
                    String id = (String)v.getTag();
                    ThirdPartyLib.requestEscort(DebugActivity.this, id);
                }catch(Exception e){
                    e.printStackTrace();
                    error(e);
                }
            }
        });
        guide_list.addView(button);
    }

    /**
     * 안내 모드 여부 검사해서 출력하기
     */
    public void btn2() {
        boolean r = ThirdPartyLib.isCruiseMode(this);
        result.setText(r + "");
    }

    /**
     * 같이 사진찍기 해보기
     */
    public void btn3() {
        ThirdPartyLib.requestPhotoService(this);
    }

    /**
     * LG 로봇으로 포커스 이동하기
     */
    public void btn4() {
        findViewById(R.id.view).postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    ThirdPartyLib.onUserInteractionTimeout(DebugActivity.this);
                }catch(Exception e){
                    e.printStackTrace();
                    error(e);
                }
            }
        }, 3000);

    }

    /**
     * 화면 해상도 보기
     */
    public  void btn5() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        int dpi = displayMetrics.densityDpi;

        result.setText("height: "+height + "width: "+width+" dpi: "+dpi);
    }
    public void error(Exception e){
        result.setText(e.getLocalizedMessage());
    }
}
